package com.gsuk.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class FourthActivityResult extends Activity {

    TextView tvBrzinaResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth_result);


        TextView enteredValue1 = (TextView) findViewById(R.id.tvMphResult) ;
        String passedArg = getIntent().getExtras().getString("arg");
        enteredValue1.setText(passedArg);

        TextView resultValue1 = (TextView) findViewById(R.id.tvKmhResult);

        Double pocetnaVrijednost1 = Double.parseDouble(passedArg);
        Double izracun1 = pocetnaVrijednost1*1.61;

        resultValue1.setText(izracun1.toString());

    }
}
