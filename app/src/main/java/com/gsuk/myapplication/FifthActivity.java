package com.gsuk.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FifthActivity extends Activity implements View.OnClickListener {

    Button btnTempResult;
    static TextView textView3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_fifth);
        this.setUpUI();

        textView3 = (TextView) findViewById(R.id.etTemp);
    }
    public void transferIT(View view)
    {
        Intent intent = new Intent(this, FifthActivityResult.class);
        intent.putExtra("arg",getText());

        startActivity(intent);
    }

    private void setUpUI ()
    {
        this.btnTempResult = (Button) findViewById(R.id.btnPretvori4);
        this.btnTempResult.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        Intent explicitIntent = new Intent();
        explicitIntent = new Intent();
        explicitIntent.putExtra("arg",getText());
        explicitIntent.setClass(getApplicationContext(), FifthActivityResult.class);
        this.startActivity(explicitIntent);
    }
    public String getText()
    {
        EditText ed4 = (EditText) findViewById(R.id.etTemp);
        return ed4.getText().toString();
    }
}

