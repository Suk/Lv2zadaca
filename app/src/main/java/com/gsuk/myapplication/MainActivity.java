package com.gsuk.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {
    Button btnVisina;
    Button btnTezina;
    Button btnTemp;
    Button btnBrzina;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        this.setUpUI();
    }
    private void setUpUI() {
        this.btnVisina = (Button) findViewById(R.id.btnVisina);
        this.btnTezina = (Button) findViewById(R.id.btnTezina);
        this.btnBrzina = (Button) findViewById(R.id.btnBrzina);
        this.btnTemp = (Button) findViewById(R.id.btnTemp);
        this.btnVisina.setOnClickListener(this);
        this.btnTemp.setOnClickListener(this);
        this.btnTezina.setOnClickListener(this);
        this.btnBrzina.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Intent explicitIntent = new Intent();
        switch (v.getId())
        {
            case (R.id.btnVisina):
                explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), SecondActivity.class);
                this.startActivity(explicitIntent);
                break;
            case (R.id.btnTezina) :
                explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), ThirdActivity.class);
                this.startActivity(explicitIntent);
                break;
            case (R.id.btnBrzina) :
                explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), FourthActivity.class);
                this.startActivity(explicitIntent);
                break;
            case (R.id.btnTemp) :
                explicitIntent = new Intent();
                explicitIntent.setClass(getApplicationContext(), FifthActivity.class);
                this.startActivity(explicitIntent);
                break;
        }

    }
}