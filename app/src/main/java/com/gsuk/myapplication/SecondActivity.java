package com.gsuk.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends Activity implements View.OnClickListener
{

    Button btnVisinaResult;
    static TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_second);
        this.setUpUI();

        textView = (TextView) findViewById(R.id.etVisina);
    }
    public void transferIT(View view)
    {
        Intent intent = new Intent(this, SecondActivityResult.class);
        intent.putExtra("arg",getText());

        startActivity(intent);
    }

    private void setUpUI ()
    {
        this.btnVisinaResult = (Button) findViewById(R.id.btnPretvori1);
        this.btnVisinaResult.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        Intent explicitIntent = new Intent();
        explicitIntent = new Intent();
        explicitIntent.putExtra("arg",getText());
        explicitIntent.setClass(getApplicationContext(), SecondActivityResult.class);
        this.startActivity(explicitIntent);
    }
    public String getText()
    {
        EditText ed1 = (EditText) findViewById(R.id.etVisina);
        return ed1.getText().toString();
    }
}
