package com.gsuk.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FourthActivity extends Activity implements View.OnClickListener {

    Button btnBrzinaResult;
    static TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_fourth);
        this.setUpUI();

        textView2 = (TextView) findViewById(R.id.etBrzina);
    }
    public void transferIT(View view)
    {
        Intent intent = new Intent(this, FourthActivityResult.class);
        intent.putExtra("arg",getText());

        startActivity(intent);
    }

    private void setUpUI ()
    {
        this.btnBrzinaResult = (Button) findViewById(R.id.btnPretvori3);
        this.btnBrzinaResult.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        Intent explicitIntent = new Intent();
        explicitIntent = new Intent();
        explicitIntent.putExtra("arg",getText());
        explicitIntent.setClass(getApplicationContext(), FourthActivityResult.class);
        this.startActivity(explicitIntent);
    }
    public String getText()
    {
        EditText ed3 = (EditText) findViewById(R.id.etBrzina);
        return ed3.getText().toString();
    }
}
