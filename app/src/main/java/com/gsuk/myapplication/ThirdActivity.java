package com.gsuk.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ThirdActivity extends Activity implements View.OnClickListener {

    Button btnTezinaResult;
    static TextView textView1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_third);
        this.setUpUI();

        textView1 = (TextView) findViewById(R.id.etMasa);
    }
    public void transferIT(View view)
    {
        Intent intent = new Intent(this, ThirdActivityResult.class);
        intent.putExtra("arg",getText());

        startActivity(intent);
    }

    private void setUpUI ()
    {
        this.btnTezinaResult = (Button) findViewById(R.id.btnPretvori2);
        this.btnTezinaResult.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        Intent explicitIntent = new Intent();
        explicitIntent = new Intent();
        explicitIntent.putExtra("arg",getText());
        explicitIntent.setClass(getApplicationContext(), ThirdActivityResult.class);
        this.startActivity(explicitIntent);
    }
    public String getText()
    {
        EditText ed2 = (EditText) findViewById(R.id.etMasa);
        return ed2.getText().toString();
    }
}
