package com.gsuk.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivityResult extends Activity {

    TextView tvVisinaResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_result);


        TextView enteredValue = (TextView) findViewById(R.id.tvCmResult) ;
        String passedArg = getIntent().getExtras().getString("arg");
        enteredValue.setText(passedArg);

        TextView resultValue = (TextView) findViewById(R.id.tvMResult);

        Float pocetnaVrijednost = Float.parseFloat(passedArg);
        Float izracun = pocetnaVrijednost / 100;

        resultValue.setText(izracun.toString());

    }



}
