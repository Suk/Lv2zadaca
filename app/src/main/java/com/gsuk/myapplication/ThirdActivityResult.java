package com.gsuk.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ThirdActivityResult extends Activity {

    TextView tvMasaResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_result);


        TextView enteredValue1 = (TextView) findViewById(R.id.tvGResult) ;
        String passedArg = getIntent().getExtras().getString("arg");
        enteredValue1.setText(passedArg);

        TextView resultValue1 = (TextView) findViewById(R.id.tvKgResult);

        Float pocetnaVrijednost1 = Float.parseFloat(passedArg);
        Float izracun1 = pocetnaVrijednost1 / 1000;

        resultValue1.setText(izracun1.toString());
    }
}
